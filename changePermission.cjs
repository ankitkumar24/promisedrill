const fs = require('fs');
const filePath = './read.txt';

function changeFilePermissions(filePath, permission) {
  return new Promise((resolve, reject) => {
    fs.chmod(filePath, permission, (err) => {
      if (err) {
        reject('permissin not changed');
      } else {
        resolve();
      }
    });
  });
}

// Usage:
changeFilePermissions(filePath, '700')
  .then(() => {
    console.log('File permissions changed successfully.');
  })
  .catch((err) => {
    console.error(err);
  });
