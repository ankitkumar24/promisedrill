const  fs = require('fs');
const filePath = './write.txt';
const data = 'hello how are you man';
function writeToFile(filePath, data){
    return new Promise((resolve, reject) =>{
        fs.writeFile(filePath,data,'utf8', (err) =>{
            if(err){
                reject('data not written');
            }
            else{
                resolve('data written');
                // reject('data not written')
            }
        });
    });
}

writeToFile(filePath,data).then((msg) =>{
console.log(msg);

}).catch((err) =>{
    console.log(err);
})
module.exports = writeToFile;