const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const filePath = './writeUuidV4.txt';
const readFile = require('./readFile.cjs');
const writeFile = require('./writefile.cjs');





const uuidPromise = Promise.resolve(uuidv4());
uuidPromise
  .then((uuid) => writeFile(filePath, uuid))
  .then(()=> readFile(filePath))
  .then((data)=> data.toUpperCase())
  .then((data)=> writeFile(filePath, data))
  .then(() => {
    console.log('UUID v4 written to file successfully.');
  })
  .catch((error) => {
    console.error(error);
  });
