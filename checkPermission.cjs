const fs = require('fs');
const filePath = './read.txt';

function checkFilePermissions(filePath) {
    return new Promise((resolve, reject) => {
        fs.access(filePath, fs.constants.R_OK | fs.constants.W_OK | fs.constants.X_OK , (err) => {
            if (err) {
                reject('permission denied');
            } else {
                resolve();
            }
        });
    });
}

// Usage:
checkFilePermissions(filePath)
    .then(() => {
        console.log('File has read,write and execute permissions.');
    })
    .catch((error) => {
        console.error(error);
    });
