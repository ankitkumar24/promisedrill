const fs = require('fs');
const filePath = './read.txt';

function readFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                reject('data not read');
            } else {
                resolve(data);
            }
        });
    });
}
module.exports = readFile;

// Usage:
readFile(filePath)
    .then((data) => {
        console.log(data);
    })
    .catch((error) => {
        console.error(error);
    });

    module.exports = readFile;
